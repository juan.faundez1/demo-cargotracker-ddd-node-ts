
// Infrastructure
import "./infra/http/app"  // Initialization
import "./infra/sequelize"

// Bounded Contexts & Subdomains 
import "./modules/booking"  // Booking Bounded Context
import "./modules/tracking" // Tracking Bounded Context