export interface TransitEdge {
    voyageNumber: string;
    fromUnLocode: string;
    toUnLocode: string;
    fromDate: string;
    toDate: string;
}