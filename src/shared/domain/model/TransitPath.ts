import { TransitEdge } from "./TransitEdge";

export interface TransitPath {
    transitEdges: Array<TransitEdge>;
}