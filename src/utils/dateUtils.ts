import { Result } from "../core/logic/Result";
import moment from 'moment';

export class DateUtils {
  public static parse (string: string): Result<Date> {
    let day = moment(string, 'YYYY-MM-DD')
    
    if (!day.isValid()) {
      return Result.fail("Parsing Date");
    }

    return Result.ok<Date>(day.toDate());
  }
}
