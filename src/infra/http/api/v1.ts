
import express from 'express'
import { bookingRouter } from '../../../modules/booking/infrastructure/http/routes';
import { routingRouter } from '../../../modules/routing/infrastructure/http/router';

const v1Router = express.Router();

v1Router.use('/cargobooking', bookingRouter);
v1Router.use('/cargorouting', routingRouter);

// All routes go here 

export { v1Router }