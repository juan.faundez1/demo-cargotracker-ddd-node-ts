
module.exports = function(sequelize, DataTypes) {
    const Location = sequelize.define('location', {
      location_id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true
      }
    });

    Location.associate = (models) => {
        Location.belongsTo(models.BaseCargo, { foreignKey: 'location_id', targetKey: 'spec_origin_id', as: 'OriginBaseCargo' })
        Location.belongsTo(models.BaseCargo, { foreignKey: 'location_id', targetKey: 'spec_destination_id', as: 'DestinationBaseCargo' })
    }
  
    return Location;
  };
  