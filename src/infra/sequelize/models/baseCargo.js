
module.exports = function(sequelize, DataTypes) {
    const BaseCargo = sequelize.define('base_cargo', {
      base_cargo_id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true
      },
      spec_origin_id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: false
      },
      spec_destination_id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: false
      },
      spec_arrival_deadline: {
        type: DataTypes.DATE,
        allowNull: false
      },
      booking_amount: {
        type: DataTypes.INTEGER,
        allowNull: false
      },
    },{
      timestamps: true,
      underscored: true, 
      tableName: 'base_cargo',
      indexes: [
        { unique: true, fields: ['base_cargo_id'] },
      ]
    });
  
    BaseCargo.associate = (models) => {
      BaseCargo.hasOne(models.Location, { as: 'OriginBaseCargo', foreignKey: 'spec_origin_id' })
      BaseCargo.hasOne(models.Location, { as: 'DestinationBaseCargo', foreignKey: 'spec_destination_id' })
    }
  
    return BaseCargo;
  };
  