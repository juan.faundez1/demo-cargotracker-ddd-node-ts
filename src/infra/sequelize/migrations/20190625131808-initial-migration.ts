'use strict';
import runner from '../runner'

export default {
  up: (queryInterface, Sequelize) => {
    const CREATE_BASE_CARGO = () => (
      queryInterface.createTable('base_cargo', {
        base_cargo_id: {
          type: Sequelize.UUID,
          allowNull: false,
          primaryKey: true
        },
        booking_amount: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        spec_origin_id: {
          type: Sequelize.STRING,
          allowNull: false,
          references: {
            model: 'location',
            key: 'location_id'
          }
        },
        spec_destination_id: {
          type: Sequelize.STRING,
          allowNull: false,
          references: {
            model: 'location',
            key: 'location_id'
          }
        },
        spec_arrival_deadline: {
          type: Sequelize.DATE,
          allowNull: false
        },
        created_at: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        },
        updated_at: {
          type: Sequelize.DATE,
          allowNull: false,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        }
      })
    )

    const CREATE_LOCATION = () => (
      queryInterface.createTable('location', {
        location_id: {
          type: Sequelize.STRING,
          allowNull: false,
          primaryKey: true
        },
      })
    )

    return runner.run([
      () => CREATE_LOCATION(),
      () => CREATE_BASE_CARGO()
    ])
  },

  down: (queryInterface, Sequelize) => {
    return runner.run([
      () => queryInterface.dropTable('base_cargo'),
      () => queryInterface.dropTable('location')
    ])
  }
};
