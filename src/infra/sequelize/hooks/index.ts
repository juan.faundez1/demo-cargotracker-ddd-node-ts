
import models from '../models';
import { DomainEvents } from '../../../core/domain/events/DomainEvents';
import { UniqueEntityID } from '../../../core/domain/UniqueEntityID';

const dispatchEventsCallback = (model: any, primaryKeyField: string) => {
  const aggregateId = new UniqueEntityID(model[primaryKeyField]);
  DomainEvents.dispatchEventsForAggregate(aggregateId);
}

(async function createHooksForAggregateRoots () {

  const { BaseCargo } = models;

  BaseCargo.addHook('afterCreate', (m: any) => dispatchEventsCallback(m, 'base_cargo_id'));
  BaseCargo.addHook('afterDestroy', (m: any) => dispatchEventsCallback(m, 'base_cargo_id'));
  BaseCargo.addHook('beforeUpdate', (m: any) => dispatchEventsCallback(m, 'base_cargo_id'));
  BaseCargo.addHook('afterSave', (m: any) => dispatchEventsCallback(m, 'base_cargo_id'));
  BaseCargo.addHook('afterUpsert', (m: any) => dispatchEventsCallback(m, 'base_cargo_id'));

})();