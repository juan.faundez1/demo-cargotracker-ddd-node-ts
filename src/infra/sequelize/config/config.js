require('dotenv').config()
const Sequelize = require('sequelize');

const { 
  CARGO_TRACKER_DB_USER, 
  CARGO_TRACKER_DB_PASS, 
  CARGO_TRACKER_DB_HOST,
  CARGO_TRACKER_DB_DEV_DB_NAME,
  CARGO_TRACKER_DB_TEST_DB_NAME,
  CARGO_TRACKER_DB_PROD_DB_NAME,
  NODE_ENV
} = process.env;

const databaseCredentials = {
  "development": {
    "username": CARGO_TRACKER_DB_USER,
    "password": CARGO_TRACKER_DB_PASS,
    "database": CARGO_TRACKER_DB_DEV_DB_NAME,
    "host": CARGO_TRACKER_DB_HOST,
    "dialect": "mysql"
  },
  "test": {
    "username": CARGO_TRACKER_DB_USER,
    "password": CARGO_TRACKER_DB_PASS,
    "database": CARGO_TRACKER_DB_TEST_DB_NAME,
    "host": CARGO_TRACKER_DB_HOST,
    "dialect": "mysql"
  },
  "production": {
    "username": CARGO_TRACKER_DB_USER,
    "password": CARGO_TRACKER_DB_PASS,
    "database": CARGO_TRACKER_DB_PROD_DB_NAME,
    "host": CARGO_TRACKER_DB_HOST,
    "dialect": "mysql"
  }
};

const { username, password, database, host, dialect } = databaseCredentials[NODE_ENV];

module.exports = databaseCredentials;

module.exports.connection = new Sequelize(database, username, password, {
  host,
  dialect,
  port: 3306,
  dialectOptions: {
    multipleStatements: true,
  },
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  logging: false
});
