import { left, right , Result } from "../../../../../core/logic/Result";
import { TransitEdge } from "../../../../../shared/domain/model/TransitEdge";
import { RouteSpecification } from "../../../domain/model/valueobjects/routeSpecification";

import { TransitPath } from "../../../../../shared/domain/model/TransitPath";
import { Leg } from "../../../domain/model/valueobjects/Leg";
import { Delivery } from "../../../domain/model/valueobjects/Delivery";
import { DateUtils } from "../../../../../utils/dateUtils";

import * as axios from 'axios';

export interface IExternalCargoRoutingService {
    fetchRouteForSpecification (routeSpecification: RouteSpecification): Promise<Delivery>;
}

export class ExternalCargoRoutingService implements IExternalCargoRoutingService{
    async fetchRouteForSpecification (routeSpecification: RouteSpecification): Promise<Delivery> {
        try {
            // NOTE: Won't send routeSpecification for simplicity (cargorouting is hard coded)
            
            const { data  } = await axios.default.get('http://localhost:9044/api/v1/cargorouting');

            const legs = data.transitPath.transitEdges.map(edge => { return this.toLeg(edge); });

            const delivery = Delivery.create(legs);

            return await delivery;
          } catch (error) {
            console.error(error);
            return error;
          }
    }

    /* Anti-Corruption Layer */
    private toLeg (edge: TransitEdge): Leg {
        return Leg.create({
            voyageNumber: edge.voyageNumber,
            toUnLocode: edge.toUnLocode,
            fromUnLocode: edge.fromUnLocode,
            toDate: DateUtils.parse(edge.toDate).getValue(),
            fromDate: DateUtils.parse(edge.fromDate).getValue()
        }).getValue();
    }
}