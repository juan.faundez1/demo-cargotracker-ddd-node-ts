import { UseCase } from "../../../../../core/domain/UseCase";
import { Either, Result, left, right } from "../../../../../core/logic/Result";
import { GenericAppError } from "../../../../../core/logic/AppError";
import { ICargoRepo } from "../../../infrastructure/repositories/cargoRepo";
import { IExternalCargoRoutingService } from "../outboundservices/ExternalCargoRoutingService";
import { BookingId } from "../../../domain/model/aggregates/bookingId";

type Response = Either<GenericAppError.UnexpectedError,  Result<any>>

export class CargoRoutingCommandService implements UseCase<BookingId, Promise<Response>> {
    private cargoRepo: ICargoRepo;
    private externalCargoRoutingService: IExternalCargoRoutingService;

    constructor (cargoRepo: ICargoRepo, externalCargoRoutingService: IExternalCargoRoutingService) {
        this.cargoRepo = cargoRepo;
        this.externalCargoRoutingService = externalCargoRoutingService;
    }

    async execute (bookingId: BookingId): Promise<Response> {
        try {
            // Retrieve Aggregate state
            const cargo = await this.cargoRepo.findUserByBookingId(bookingId.id.toString())

            // Get external data 
            const delivery = await this.externalCargoRoutingService.fetchRouteForSpecification(cargo.routeSpecification);
            
            // Change Aggregate state and register Domain Event
            await cargo.assignRoute(delivery);
            
            this.cargoRepo.update(cargo);

            return right(Result.ok<any>(delivery))
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err));
        }
    }
}