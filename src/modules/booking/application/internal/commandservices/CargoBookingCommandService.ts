import { UseCase } from "../../../../../core/domain/UseCase";
import { Either, Result, left, right } from "../../../../../core/logic/Result";
import { GenericAppError } from "../../../../../core/logic/AppError";
import { v4 as uuidv4 } from "uuid";
import { Cargo } from "../../../domain/model/aggregates/cargo";
import { ICargoRepo } from "../../../infrastructure/repositories/cargoRepo";
import { UniqueEntityID } from "../../../../../core/domain/UniqueEntityID";
import { BookCargoCommand } from "../../../domain/model/commands/BookCargoCommand";

type Response = Either<GenericAppError.UnexpectedError,  Result<any>>

/* Input Port */
export class CargoBookingCommandService implements UseCase<BookCargoCommand, Promise<Response>> {
    private cargoRepo: ICargoRepo;

    constructor (cargoRepo: ICargoRepo) {
        this.cargoRepo = cargoRepo;
    }

    async execute (command: BookCargoCommand): Promise<Response> {
        /* 1: Business Id */
        const bookingId = uuidv4()
        /* 2: Create Aggregate */
        const cargoOrError = Cargo.create(command, new UniqueEntityID(bookingId));

        if (cargoOrError.isFailure) {
            return left(Result.fail(cargoOrError.error));
        }
    
        const cargo: Cargo = cargoOrError.getValue();

        try {
            /* Saving to Datastore triggers Transaction Handlers publishing the registered Domain Event */
            await this.cargoRepo.save(cargo);
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err));
        }
        
        return right(Result.ok<any>(bookingId))
    }
}