import { UseCase } from "../../../../../core/domain/UseCase";
import { Either, Result, left, right } from "../../../../../core/logic/Result";
import { GenericAppError } from "../../../../../core/logic/AppError";
import { v4 as uuidv4 } from "uuid";
import { CreateCargoDTO } from "../../../interfaces/dto/CreateCargoDTO";
import { Cargo } from "../../../domain/model/aggregates/cargo";
import { ICargoRepo } from "../../../infrastructure/repositories/cargoRepo";
import { UniqueEntityID } from "../../../../../core/domain/UniqueEntityID";
import { BookCargoCommand } from "../../../domain/model/commands/BookCargoCommand";
import { BookingId } from "../../../domain/model/aggregates/bookingId";

type Response = Either<GenericAppError.UnexpectedError,  Result<any>>

export class CargoBookingQueryService implements UseCase<BookingId, Promise<Response>> {
    private cargoRepo: ICargoRepo;

    constructor (cargoRepo: ICargoRepo) {
        this.cargoRepo = cargoRepo;
    }

    async execute (bookingId: BookingId): Promise<Response> {
        try {
            const cargo = await this.cargoRepo.findUserByBookingId(bookingId.id.toString())
            return right(Result.ok<any>(cargo))
        } catch (err) {
            return left(new GenericAppError.UnexpectedError(err));
        }
    }
}