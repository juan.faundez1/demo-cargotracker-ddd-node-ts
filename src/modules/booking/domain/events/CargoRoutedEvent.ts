import { IDomainEvent } from "../../../../core/domain/events/IDomainEvent";
import { UniqueEntityID } from "../../../../core/domain/UniqueEntityID";
import { BookingId } from "../model/aggregates/bookingId";

export class CargoRoutedEvent implements IDomainEvent {
  public dateTimeOccurred: Date;
  public bookingId: BookingId;

  constructor (bookingId: BookingId) {
    this.dateTimeOccurred = new Date();
    this.bookingId = bookingId;
  }
  
  getAggregateId (): UniqueEntityID {
    return this.bookingId.id;
  }
}