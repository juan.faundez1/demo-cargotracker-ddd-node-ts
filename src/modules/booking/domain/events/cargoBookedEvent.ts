import { IDomainEvent } from "../../../../core/domain/events/IDomainEvent";
import { UniqueEntityID } from "../../../../core/domain/UniqueEntityID";
import { BookingId } from "../model/aggregates/bookingId";

export class CargoBookedEvent implements IDomainEvent {
  public dateTimeOccurred: Date;
  public bookingId: BookingId;
  public data: any;

  constructor (bookingId: BookingId, data: any) {
    this.dateTimeOccurred = new Date();
    this.bookingId = bookingId;
    this.data = data;
  }
  
  getAggregateId (): UniqueEntityID {
    return this.bookingId.id;
  }
}