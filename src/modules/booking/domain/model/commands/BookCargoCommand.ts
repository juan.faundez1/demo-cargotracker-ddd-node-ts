export interface BookCargoCommand {
    bookingId: string,
    bookingAmount: string;
    originLocation: string;
    destinationLocation: string;
    arrivalDeadline: string;
}

