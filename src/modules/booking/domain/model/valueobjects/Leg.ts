import { setEmitFlags } from "typescript";
import { Entity } from "../../../../../core/domain/Entity";
import { UniqueEntityID } from "../../../../../core/domain/UniqueEntityID";
import { Guard } from "../../../../../core/logic/Guard";
import { Result } from "../../../../../core/logic/Result";

export interface LegProps  {
    voyageNumber: string;
    fromUnLocode: string;
    toUnLocode: string;
    fromDate: Date;
    toDate: Date;
}

export class Leg extends Entity<LegProps> {

    get id (): UniqueEntityID {
        return this._id;
    }

    get voyageNumber (): string {
        return this.props.voyageNumber;
    }

    get fromUnLocode (): string {
        return this.props.fromUnLocode;
    }

    get toUnLocode (): string {
        return this.props.toUnLocode;
    }

    get fromDate (): Date {
        return this.props.fromDate;
    }

    get toDate (): Date {
        return this.props.toDate;
    }

    private constructor (props?: LegProps) {
        super(props, null)
    }

    public static create (props?: LegProps): Result<Leg> {
        return Result.ok<Leg>(new Leg({...props}));
    }
}