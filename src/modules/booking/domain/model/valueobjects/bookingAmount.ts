
import { ValueObject } from "../../../../../core/domain/ValueObject";
import { Result } from "../../../../../core/logic/Result";
import { Guard } from "../../../../../core/logic/Guard";

interface BookingAmountProps {
  value: string;
}

export class BookingAmount extends ValueObject<BookingAmountProps> {
  get value (): string {
    return this.props.value;
  }
  
  private constructor (props: BookingAmountProps) {
    super(props);
  }

  public static create (amount: string): Result<BookingAmount> {
    const guardResult = Guard.againstNullOrUndefined(amount, 'amount');
    if (!guardResult.succeeded) {
      return Result.fail<BookingAmount>(guardResult.message);
    } else {
      return Result.ok<BookingAmount>(new BookingAmount({ value: amount }))
    }
  }
}