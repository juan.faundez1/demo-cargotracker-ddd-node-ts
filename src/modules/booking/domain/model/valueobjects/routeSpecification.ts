import { ValueObject } from "../../../../../core/domain/ValueObject";
import { Result } from "../../../../../core/logic/Result";
import { Guard } from "../../../../../core/logic/Guard";
import { Location } from "../entities/location";

interface RouteSpecificationProps {
  originLocation: Location;
  destinationLocation: Location;
  arrivalDeadline: Date;
}

export class RouteSpecification extends ValueObject<RouteSpecificationProps> {
  get origin (): Location {
    return this.props.originLocation;
  }

  get destination (): Location {
    return this.props.destinationLocation;
  }

  get arrivalDeadline (): Date {
    return this.props.arrivalDeadline;
  }
  
  private constructor (props: RouteSpecificationProps) {
    super(props);
  }

  public static create (props: RouteSpecificationProps): Result<RouteSpecification> {
    const guardedProps = [
        { argument: props.originLocation, argumentName: 'originLocation' },
        { argument: props.destinationLocation, argumentName: 'destinationLocation' },
        { argument: props.arrivalDeadline, argumentName: 'arrivalDeadline' }
    ];

    const guardResult = Guard.againstNullOrUndefinedBulk(guardedProps);

    if (!guardResult.succeeded) {
      return Result.fail<RouteSpecification>(guardResult.message);
    } else {
      return Result.ok<RouteSpecification>(new RouteSpecification(props))
    }
  }
}