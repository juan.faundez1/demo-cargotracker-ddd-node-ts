
import { ValueObject } from "../../../../../core/domain/ValueObject";
import { Result } from "../../../../../core/logic/Result";
import { Leg } from "./Leg";

interface DeliveryProps {
  value: Array<Leg>;
}

export class Delivery extends ValueObject<DeliveryProps> {
  get value (): Array<Leg> {
    return this.props.value;
  }
  
  private constructor (props: DeliveryProps) {
    super(props);
  }

  public static create (legs: Array<Leg>): Delivery {
    return new Delivery({ value: legs });
  }
}