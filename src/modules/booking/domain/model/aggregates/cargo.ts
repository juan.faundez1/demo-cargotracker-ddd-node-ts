import { AggregateRoot } from "../../../../../core/domain/AggregateRoot";
import { UniqueEntityID } from "../../../../../core/domain/UniqueEntityID";
import { Result } from "../../../../../core/logic/Result";
import { BookingId } from "./bookingId";
import { CargoBookedEvent } from "../../events/cargoBookedEvent";
import { BookingAmount } from "../valueobjects/bookingAmount";
import { Location } from "../entities/location";
import { RouteSpecification } from "../valueobjects/routeSpecification";
import { DateUtils } from "../../../../../utils/dateUtils";
import { BookCargoCommand } from "../commands/BookCargoCommand";
import { Delivery } from "../valueobjects/Delivery";
import { cargoRoutingUseCase } from "../../../useCases/assignCargoRoute";
import { CargoRoutedEvent } from "../../events/CargoRoutedEvent";

interface CargoProps {
  bookingAmount: BookingAmount;
  originLocation: Location;
  routeSpecification: RouteSpecification;
  delivery: Delivery;
}

export class Cargo extends AggregateRoot<CargoProps> {
    get id (): UniqueEntityID {
        return this._id;
    }

    get bookingId (): BookingId {
        return new BookingId(this.id)
    }

    get bookingAmount (): BookingAmount {
        return this.props.bookingAmount;
    }

    get originLocation (): Location {
        return this.props.routeSpecification.origin
    }

    get routeSpecification (): RouteSpecification {
        return this.props.routeSpecification;
    }

    get delivery (): Delivery {
        return this.props.delivery;
    }

    public constructor (props: CargoProps, id?: UniqueEntityID) {
        super(props, id);
    }

    /* Command Handlers */
    
    public static create (command: BookCargoCommand, id?: UniqueEntityID): Result<Cargo> {
        const bookingAmountOrError = BookingAmount.create(command.bookingAmount);
        const originLocationOrError = Location.create(command.originLocation);
        const destinationLocationOrError = Location.create(command.destinationLocation);
        const arrivalDeadlineOrError = DateUtils.parse(command.arrivalDeadline);

        const combinedPropsResult = Result.combine([ 
            bookingAmountOrError, 
            originLocationOrError, 
            destinationLocationOrError,
            arrivalDeadlineOrError
        ]);
        
        if (combinedPropsResult.isFailure) {
            return Result.fail<Cargo>(combinedPropsResult.error);
        }

        const props = {
            delivery: null,
            bookingAmount: bookingAmountOrError.getValue(),
            originLocation: originLocationOrError.getValue(),
            routeSpecification: RouteSpecification.create({ 
                originLocation: originLocationOrError.getValue(),
                destinationLocation: destinationLocationOrError.getValue(),
                arrivalDeadline: arrivalDeadlineOrError.getValue()
            },).getValue()
        };

        const cargo = new Cargo({ ...props, }, id);
  
        const idWasProvided = !!id;
        if (idWasProvided) {
            /* 3: Register Domain Event */
            cargo.addDomainEvent(
                new CargoBookedEvent(cargo.bookingId, {
                    bookingId: cargo.id.toValue(),
                    bookingAmount: cargo.bookingAmount.value,
                    routeSpecification: JSON.stringify(cargo.routeSpecification.props)
                }));
            }
  
        return Result.ok<Cargo>(cargo);
    }

    public assignRoute(delivery: Delivery): void {
        this.props.delivery = delivery;

        this.addDomainEvent(new CargoRoutedEvent(this.bookingId));
    }
}