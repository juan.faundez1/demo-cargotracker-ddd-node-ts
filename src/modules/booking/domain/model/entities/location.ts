import { Entity } from "../../../../../core/domain/Entity";
import { UniqueEntityID } from "../../../../../core/domain/UniqueEntityID";
import { Guard } from "../../../../../core/logic/Guard";
import { Result } from "../../../../../core/logic/Result";

export class Location extends Entity<String> {

    get id (): UniqueEntityID {
        return this._id;
    }

    private constructor (id?: UniqueEntityID) {
        super(null, id)
    }

    public static create (id?: string): Result<Location> {
        const guardResult = Guard.againstNullOrUndefined(id, 'id');

        if (!guardResult.succeeded) {
            return Result.fail<Location>(guardResult.message);
        } else {
            return Result.ok<Location>(new Location(new UniqueEntityID(id)))
        }
  }
}