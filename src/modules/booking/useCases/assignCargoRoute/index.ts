
import { AssignCargoRouteController } from "../../interfaces/rest/AssignCargoRouteController";
import { CargoRoutingCommandService } from "../../application/internal/commandservices/CargoRoutingCommandService";
import { cargoRepo } from "../../infrastructure/repositories";
import { ExternalCargoRoutingService } from "../../application/internal/outboundservices/ExternalCargoRoutingService";

const externalCargoRoutingService = new ExternalCargoRoutingService();
const cargoRoutingUseCase = new CargoRoutingCommandService(cargoRepo, externalCargoRoutingService);
const assignCargoRouteController = new AssignCargoRouteController(cargoRoutingUseCase);

export {
  cargoRoutingUseCase,
  assignCargoRouteController
}