import { ViewCargoDetailsController } from "../../interfaces/rest/ViewCargoDetailsController";
import { CargoBookingQueryService } from "../../application/internal/queryservices/CargoBookingQueryServices";
import { cargoRepo } from "../../infrastructure/repositories";

const viewCargoDetailsUseCase = new CargoBookingQueryService(cargoRepo);
const viewCargoDetailsController = new ViewCargoDetailsController(viewCargoDetailsUseCase)

export {
    viewCargoDetailsUseCase,
    viewCargoDetailsController
}