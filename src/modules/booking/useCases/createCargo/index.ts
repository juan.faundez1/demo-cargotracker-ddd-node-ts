
import { CreateCargoController } from "../../interfaces/rest/CreateCargoController";
import { CargoBookingCommandService } from "../../application/internal/commandservices/CargoBookingCommandService";
import { cargoRepo } from "../../infrastructure/repositories";

const createCargoUseCase = new CargoBookingCommandService(cargoRepo);
const createCargoController = new CreateCargoController(createCargoUseCase)

export {
  createCargoUseCase,
  createCargoController
}