import { BookCargoCommand } from "../../domain/model/commands/BookCargoCommand";

export class BookCargoCommandDTOAssembler {
    public static toCommanndFromDTO (dto: any): BookCargoCommand {
        return {
            bookingId: null,
            bookingAmount: dto.bookingAmount,
            originLocation: dto.originLocation,
            destinationLocation: dto.destinationLocation,
            arrivalDeadline: dto.arrivalDeadline
        };
    }
}