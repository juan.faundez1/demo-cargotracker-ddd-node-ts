
import { BaseController } from "../../../../core/infra/BaseController";
import { CargoRoutingCommandService } from "../../application/internal/commandservices/CargoRoutingCommandService";
import { BookingId } from "../../domain/model/aggregates/bookingId";

export class AssignCargoRouteController extends BaseController {
  private useCase: CargoRoutingCommandService;

  constructor (useCase: CargoRoutingCommandService) {
    super();
    this.useCase = useCase;
  }

  async executeImpl (): Promise<any> {
    const dto = this.req.body

    try {
      const result = await this.useCase.execute(new BookingId(dto.bookingId));
      
      if (result.isLeft()) {
        const error = result.value;
        return this.fail(error.errorValue().message);
      } else {
        return this.ok(this.res, {cargo: result.value.getValue()});
      }
    } catch (err) {
      return this.fail(err)
    }
  }
}