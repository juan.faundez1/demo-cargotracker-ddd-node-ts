
import { BaseController } from "../../../../core/infra/BaseController";
import { CreateCargoDTO } from "../dto/CreateCargoDTO";
import { CargoBookingCommandService } from "../../application/internal/commandservices/CargoBookingCommandService";
import { BookCargoCommandDTOAssembler } from "../transform/BookCargoCommandDTOAssembler";

/* Input Adapter */
export class CreateCargoController extends BaseController {
  private useCase: CargoBookingCommandService;

  constructor (useCase: CargoBookingCommandService) {
    super();
    this.useCase = useCase;
  }

  async executeImpl (): Promise<any> {
    const dto: CreateCargoDTO = this.req.body as CreateCargoDTO;
    const command = BookCargoCommandDTOAssembler.toCommanndFromDTO(dto)

    try {
      const result = await this.useCase.execute(command);
      
      if (result.isLeft()) {
        const error = result.value;
        return this.fail(error.errorValue().message);
      } else {
        return this.ok(this.res, {bookingId: result.value.getValue()});
      }
    } catch (err) {
      return this.fail(err)
    }
  }
}