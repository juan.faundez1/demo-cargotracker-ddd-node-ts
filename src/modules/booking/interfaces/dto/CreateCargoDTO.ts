export interface CreateCargoDTO {
    bookingAmount: string;
    originLocation: string;
    destinationLocation: string;
    arrivalDeadline: string;
}
