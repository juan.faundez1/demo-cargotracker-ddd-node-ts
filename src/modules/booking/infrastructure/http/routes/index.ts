
import express from 'express';
import { createCargoController } from '../../../useCases/createCargo';
import { viewCargoDetailsController } from '../../../useCases/viewCargoDetails';
import { assignCargoRouteController } from '../../../useCases/assignCargoRoute';

const bookingRouter = express.Router();

bookingRouter.post('/', (req, res) => createCargoController.execute(req, res) )
bookingRouter.get('/', (req, res) => viewCargoDetailsController.execute(req, res) )
bookingRouter.get('/assignroute', (req, res) => assignCargoRouteController.execute(req, res) )

export { bookingRouter };