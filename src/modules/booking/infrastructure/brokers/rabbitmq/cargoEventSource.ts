import { IHandle } from "../../../../../core/domain/events/IHandle";
import { CargoBookedEvent } from "../../../domain/events/cargoBookedEvent";
import { DomainEvents } from "../../../../../core/domain/events/DomainEvents";
import { CargoRoutedEvent } from "../../../domain/events/CargoRoutedEvent";

import * as amqp from 'amqplib'

export class CargoEventSource implements IHandle<CargoBookedEvent> {
  private channel: amqp.Channel;

  constructor (channel: amqp.Channel) {
    this.setupSubscriptions();
    this.channel = channel
  }

  setupSubscriptions(): void {
    DomainEvents.register(this.onCargoCreatedEvent.bind(this), CargoBookedEvent.name);
    DomainEvents.register(this.onCargoRoutedEvent.bind(this), CargoRoutedEvent.name);
  }

  /* Cargo Booked Event */
  private async onCargoCreatedEvent (event: CargoBookedEvent): Promise<void> {
    console.log("[Booking: Publishing Message CargoCreatedEvent to: booking queue]");
    console.log(event.data);
    this.channel.sendToQueue("booking", Buffer.from(JSON.stringify(event.data)))
  }

  /* Cargo Routed Event */
  private async onCargoRoutedEvent (event: CargoRoutedEvent): Promise<void> {
    console.log("[Booking: Publishing Message CargoRoutedEvent to: routing queue]");
    console.log(event.bookingId.id);
    this.channel.sendToQueue("routing", Buffer.from(JSON.stringify({ bookingId: event.bookingId.id.toString() })))
  }
}