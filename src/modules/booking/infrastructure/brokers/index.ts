import { CargoEventSource } from "./rabbitmq/cargoEventSource";

import * as amqp from 'amqplib'

/* Default RabittMQ Properties */

amqp.connect('amqp://localhost:5672')
    .then(connection => {
        return connection.createChannel()
            .tap(channel => channel.checkQueue('booking'))
            .tap(channel => channel.checkQueue('routing'))
            .then(channel => {
                console.log("[Subscriptions] Publisher Started");

                new CargoEventSource(channel);
            })
    });