
import { UniqueEntityID } from "../../../../../core/domain/UniqueEntityID";
import { Mapper } from "../../../../../core/infra/Mapper";
import { DateUtils } from "../../../../../utils/dateUtils";
import { Cargo } from "../../../domain/model/aggregates/cargo";
import { Location } from "../../../domain/model/entities/location";
import { BookingAmount } from "../../../domain/model/valueobjects/bookingAmount";
import { RouteSpecification } from "../../../domain/model/valueobjects/routeSpecification";

export class CargoMap extends Mapper<Cargo> {

  public static toPersistence (cargo: Cargo): any {
    return {
      base_cargo_id: cargo.id.toString(),
      booking_amount: cargo.bookingAmount.value,
      spec_origin_id: cargo.routeSpecification.origin.id.toString(),
      spec_destination_id: cargo.routeSpecification.destination.id.toString(),
      spec_arrival_deadline: cargo.routeSpecification.arrivalDeadline
    }
  }


  public static toDomain (raw: any): Cargo {
    const props = {
      delivery: null,
      bookingAmount: BookingAmount.create(raw.booking_amount).getValue(),
      originLocation: Location.create(raw.spec_origin_id).getValue(),
      routeSpecification: RouteSpecification.create({ 
          originLocation: Location.create(raw.spec_origin_id).getValue(),
          destinationLocation: Location.create(raw.spec_destination_id).getValue(),
          arrivalDeadline: DateUtils.parse(raw.spec_arrival_deadline).getValue()
      }).getValue()
    };

    const cargo = new Cargo({ ...props, }, new UniqueEntityID(raw.base_cargo_id));
    
    return cargo;
  }
  
}