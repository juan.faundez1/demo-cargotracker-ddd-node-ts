import { CargoRepo } from "./cargoRepo";
import models from "../../../../infra/sequelize/models";

const cargoRepo = new CargoRepo(models);

export {
  cargoRepo
}