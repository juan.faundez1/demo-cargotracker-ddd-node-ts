
import { Cargo } from "../../domain/model/aggregates/cargo";
import { CargoMap } from "./mappers/CargoMap";

/* Output Port */
export interface ICargoRepo {
  findUserByBookingId (bookingId: string): Promise<Cargo>;
  update(cargo: Cargo): Promise<void>;
  save(cargo: Cargo): Promise<void>;
}

/* Output Adapter */
export class CargoRepo implements ICargoRepo {
  private models: any;

  constructor (models: any) {
    this.models = models;
  }

  private createBaseQuery () {
    return {
      where: {}
    }
  }

  public async findUserByBookingId (bookingId: string): Promise<Cargo> {
    const baseQuery = this.createBaseQuery();
    baseQuery.where['base_cargo_id'] = bookingId;
    const user = await this.models.BaseCargo.findOne(baseQuery);
    if (!!user === true) 
      return CargoMap.toDomain(user);
    return null;
  }

  public async save (cargo: Cargo): Promise<void> {
    const BaseCargoModel = this.models.BaseCargo;
    const rawUser = CargoMap.toPersistence(cargo);
    
    try {
        // 4:
        await BaseCargoModel.create(rawUser);
    } catch (err) {
      console.log(err);
    }
  }

  public async update (cargo: Cargo): Promise<void> {
    const BaseCargoModel = this.models.BaseCargo;
    const rawUser = CargoMap.toPersistence(cargo);
    
    try {
        // 4:
        const sequelizeCargoInstance = await BaseCargoModel.findOne({ 
          where: { base_cargo_id: cargo.id.toString() }
        })
        
        return await sequelizeCargoInstance.update(rawUser);
    } catch (err) {
      console.log(err);
    }
  }
}