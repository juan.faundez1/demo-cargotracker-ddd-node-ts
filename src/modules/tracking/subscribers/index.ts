import * as amqp from 'amqplib'

/* Default RabittMQ Properties */

amqp.connect('amqp://localhost:5672')
    .then(connection => {
        return connection.createChannel()
            .tap(channel => channel.checkQueue('booking'))
            .tap(channel => channel.checkQueue('routing'))
            .then(channel => {
                console.log("[Subscriptions] Consumer Started ");
                channel.consume('booking', (message) => {
                    const event = JSON.parse(message.content.toString());

                    console.log("[Tracking: Consuming Message from Booking]");
                    console.log(event);
                })

                channel.consume('routing', (message) => {
                    const event = JSON.parse(message.content.toString());

                    console.log("[Tracking: Consuming Message from Routing]");
                    console.log(event);
                })
            })
    });