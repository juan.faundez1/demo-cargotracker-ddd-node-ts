
import express from 'express';
import { createCargoRouteController } from '../../../useCases';

const routingRouter = express.Router();

routingRouter.get('/', (req, res) => createCargoRouteController.execute(req, res) )

export { routingRouter };