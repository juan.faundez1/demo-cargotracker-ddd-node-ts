
import { BaseController } from "../../../../core/infra/BaseController";
import { TransitEdge } from "../../../../shared/domain/model/TransitEdge";
import { TransitPath } from "../../../../shared/domain/model/TransitPath";

export class CreateCargoRouteController extends BaseController {

  constructor () {
    super();
  }

  async executeImpl (): Promise<any> {
    try {
        const transtEdges: Array<TransitEdge> = [
            { voyageNumber: '1', fromUnLocode: 'CL000', toUnLocode: 'CL100', fromDate: '2021-03-01', toDate: '2021-04-01'},
            { voyageNumber: '2', fromUnLocode: 'CL100', toUnLocode: 'CL200', fromDate: '2021-04-01', toDate: '2021-05-01'}
        ]

        const transitPath: TransitPath = {
            transitEdges: transtEdges
        }

        return this.ok(this.res, { transitPath: transitPath });
    } catch (err) {
      return this.fail(err)
    }
  }
}