import { CreateCargoRouteController } from "../interfaces/rest/CreateCargoRouteController";

const createCargoRouteController = new CreateCargoRouteController();

export {
    createCargoRouteController
}