const mysql = require('mysql2');  

require('dotenv').config();

const { 
  CARGO_TRACKER_DB_USER, 
  CARGO_TRACKER_DB_PASS, 
  CARGO_TRACKER_DB_HOST,
  CARGO_TRACKER_DB_DEV_DB_NAME,
  CARGO_TRACKER_DB_TEST_DB_NAME,
  NODE_ENV
} = process.env;

const dbName = NODE_ENV === "development" 
  ? CARGO_TRACKER_DB_DEV_DB_NAME 
  : CARGO_TRACKER_DB_TEST_DB_NAME;

const connection = mysql.createConnection({  
  host: CARGO_TRACKER_DB_HOST,  
  user: CARGO_TRACKER_DB_USER,  
  password: CARGO_TRACKER_DB_PASS  
});  

connection.connect((err) => {
  if (err) throw err;
  connection.query(`DROP SCHEMA ${dbName}`, (err, result) => {
    if (err && err.code === "ER_DB_DROP_EXISTS") {
      console.log("Already deleted");
      process.exit(0);
    }

    if (err) throw err;

    console.log('Deleted db');
    process.exit(0);
  })
})